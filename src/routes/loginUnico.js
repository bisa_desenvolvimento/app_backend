const router =  require('express').Router();

const loginUnicoController = require('../controller/loginUnicoController');

router.get('/getUsuarios/:idSindicato/:ambiente/:idUsuario', loginUnicoController.getUsuarios);

module.exports = router;