const router =  require('express').Router();

router.use('/filiado', require('./filiado'));
router.use('/sindicato', require('./sindicato'));
router.use('/pagamento', require('./pagamento'));

router.use('/loginUnico', require('./loginUnico'));
router.use('/clube', require('./clube'));

module.exports = router;