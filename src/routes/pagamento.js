const router =  require('express').Router();

const pagamentoController = require('../controller/pagamentoController');
const authMiddleware = require('../middlewares/auth');

router.post('/recorrencia', authMiddleware, pagamentoController.pagamento);

router.get('/recorrencia/consultar/:recorrenciaId', pagamentoController.checarRecorrencia);

// router.put('/recorrencia/alterar')

module.exports = router;