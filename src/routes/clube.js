const router =  require('express').Router();

const clubeController = require('../controller/clubeController');

router.get('/', clubeController.index);

router.get('/:id', clubeController.show);

module.exports = router;