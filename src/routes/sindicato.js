const router =  require('express').Router();

const sindicatoController = require('../controller/sindicatoController');

router.get('/', sindicatoController.listSindicato);

router.get('/:id', sindicatoController.getSindicato);

module.exports = router;