const router =  require('express').Router();

const filiadoController = require('../controller/filiadoController');
const authMiddleware = require('../middlewares/auth');

router.post('/login', filiadoController.login);

router.put('/alterarSenha', authMiddleware, filiadoController.alterarSenha);

router.get('/consultarFiliado/:cpf/:idSindicato',authMiddleware, filiadoController.consultarFiliado);

module.exports = router;