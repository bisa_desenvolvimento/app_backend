const jwt = require('jsonwebtoken');
const auth = require('./config');

exports.getToken = async (usuario) => {
    return jwt.sign(
        { 
            id: usuario.id
        }, auth.secret, {expiresIn: 16000000});
}