const conexaoMoneta = require('../database/baseMoneta');

exports.getParceiros = async () => {
    const result = await conexaoMoneta('acpwforn')
        .select(
            { id: "acpwforn.FORN_Id" },
            { nomeFantasia: "acpwforn.FORN_NomeFantasia" },
            { imagem: "acpwforn.FORN_Logomarca" },
            { atividade: "tabeativ.ATIV_Descricao" },
            { email: "acpwforn.FORN_Email" },
            { foneContato: "acpwforn.FORN_FoneContato" },
            { site: "acpwforn.FORN_HomePage" },
            { observacao: "acpwforn.FORN_Observacao" },
            { vantagens: "acpwforn.FORN_Vantagens" },
            { validadeContrato: conexaoMoneta.raw("DATE_FORMAT(acpwforn.FORN_ValidadeContrato, '%d/%m/%Y')")},
            { credenciadoClube: "acpwforn.FORN_CredenciadoClube"},
            { celular: "acpwforn.FORN_FoneCelular"},
            { endereco: "acpwforn.FORN_Endereco"},
            { enderecoNumero: "acpwforn.FORN_Numero"},
            { bairro: "acpwforn.FORN_Bairro" },
            { cidade: "tabecidades.nome" },
            { uf: "tabecidades.uf"}
        )
        .innerJoin('tabeativ', 'tabeativ.ATIV_Codigo', 'acpwforn.FORN_ATIVIDADE')
        .innerJoin('tabecidades', 'acpwforn.FORN_Cidade', 'tabecidades.id')
        .where('acpwforn.FORN_Visivel', 1)
        .where('acpwforn.FORN_CredenciadoClube', '<>', 'N')
        .orderBy('tabeativ.ATIV_Descricao');

    return result;
}

exports.findOne = async (id) => {
    const result = await conexaoMoneta('acpwforn')
        .select(
            { id: "acpwforn.FORN_Id" },
            { nomeFantasia: "acpwforn.FORN_NomeFantasia" },
            { descricao: "acpwforn.FORN_Observacao" },
            { email: "acpwforn.FORN_Email" },
            { foneContato: "acpwforn.FORN_FoneContato" },
            { site: "acpwforn.FORN_HomePage" },
            { imagem: "acpwforn.FORN_Logomarca" },
            { vantagens: "acpwforn.FORN_Vantagens" },
            conexaoMoneta.raw('DATE_FORMAT(acpwforn.FORN_ValidadeContrato, "%d/%m/%Y") as validadeContrato'),
            { atividade: "tabeativ.ATIV_Descricao" }
        )
        .innerJoin('tabeativ', 'tabeativ.ATIV_Codigo', 'acpwforn.FORN_ATIVIDADE')
        .where('acpwforn.FORN_Visivel', 1)
        .where('acpwforn.FORN_Id', id)
        .where('acpwforn.FORN_CredenciadoClube', '<>', 'N')
        .first();

    return result;
}