const conexaoMoneta = require('../database/baseMoneta');

exports.getSindicato = async(idSindicato) => {
    const result = await conexaoMoneta('acrwclie')
        .select(
            {id: "CLIE_Codigo"},
            {nome: "CLIE_Nome"},
            {endereco: "CLIE_Endereco"},
            {numero: "CLIE_Numero"},
            {complemento: "CLIE_Complemento"},
            {bairro: "CLIE_Bairro"},
            {telefone: "CLIE_FoneContato"},
            {email: "CLIE_Email"},
            {sigla: "CLIE_Sigla"},
            {imagem: "CLIE_Logo_App"},
            {imagemCarteira: "CLIE_Logo_Carteira"},
            {database: "CLIE_Database"},
            {recipientId: "CLIE_Recipient_id"},
            {desconto: "CLIE_Desconto_Porcent"},
            {percentBisa: "CLIE_Bisa_Porcent"})
        .where("CLIE_Codigo", idSindicato)
        .where("CLIE_Visivel",1)
        .first();

        result.imagem = 'http://bisaweb.monetaweb.com.br/assents/' + result.imagem;
        result.imagemCarteira = 'http://bisaweb.monetaweb.com.br/assents/' + result.imagemCarteira;

        if(result){
            return result;
        } else {
            throw 'Sindicato não encontrado !!';
        }
}

exports.listSindicato = async() => {

    var result = await conexaoMoneta('acrwclie')
        .select(
            {id: "acrwclie.CLIE_Codigo"},
            {nome: "acrwclie.CLIE_Nome"},
            {imagem: "acrwclie.CLIE_Logo_App"})
        .innerJoin("vw_acrwregistrador", "acrwclie.CLIE_Codigo","vw_acrwregistrador.clie_codigo")
        .where('vw_acrwregistrador.SIST_Codigo', "FW")
        .where("acrwclie.CLIE_Visivel",1)
        .where("acrwclie.CLIE_Sigla", "!=", '')
        .where("acrwclie.CLIE_Logo_App", "!=", '')
        .where("acrwclie.CLIE_Logo_Carteira", "!=", '')
        .where("acrwclie.CLIE_Database", "!=", '')
        .where("acrwclie.CLIE_Recipient_id", "!=", '')
        

        result.forEach(res => {
            if(res.imagem){
                res.imagem = 'http://bisaweb.monetaweb.com.br/assents/' + res.imagem;
            }
        });

        if(result){
            return result;
        } else {
            throw 'Erro ao listar os Sindicatos !!';
        }
}