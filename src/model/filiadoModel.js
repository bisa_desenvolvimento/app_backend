const conexao = require('../database/baseFiliados');
const sindicatoModel = require('./sindicatoModel');
const crypto = require('crypto');

exports.getFiliado = async (cpf, database) => {

	const conexaoFiliados = conexao.criarConexaoFiliados(database);

	const retorno = await conexaoFiliados('documento')
		.select(
			{ dataFiliacao: "dadossindicato.DadosSindicato_DataFiliacao" },
			{ nome: "pessoafisica.PessoaFisica_Nome" },
			{ documento: "documento.Documento_Valor" },
			{ id: "filiados.filiado_id" },
			{ empresa: "empresa.Empresa_Nome" },
			{ recorrenciaId: "documento.Documento_Filiado_Recorrencia" },
			{ enderecoRua: "endereco.Endereco_Rua"},
			{ enderecoBairro: "endereco.Endereco_Bairro"},
			{ enderecoNumero: "endereco.Endereco_Numero"},
			{ enderecoComplemento: "endereco.Endereco_Complemento"},
			{ enderecoCep: "endereco.Endereco_Cep"},
			{ enderecoCidade: "cidadeestado.CidadeEstado_Cidade"},
			{ enderecoEstado: "cidadeestado.CidadeEstado_Estado"})
		.innerJoin("filiadoshasdocumento", "filiadoshasdocumento.Documento_Documento_Id", "documento.Documento_Id")
		.innerJoin("filiados", "filiados.Filiado_Id", "filiadoshasdocumento.Filiados_Filiado_Id")
		.innerJoin("pessoafisica", "pessoafisica.PessoaFisica_Id", "filiados.Filiado_PessoaFisica_Id")
		.innerJoin("dadossindicato", "dadossindicato.Filiados_Filiado_Id", "filiados.Filiado_Id")
		.innerJoin("filiadoshasempresa", "filiadoshasempresa.Filiados_Filiado_Id", "filiados.Filiado_Id")
		.innerJoin("empresa", "filiadoshasempresa.Empresa_Empresa_Id", "empresa.Empresa_Id")
		.innerJoin("endereco", "endereco.Endereco_Id", "pessoafisica.PessoaFisica_Endereco_Id")
		.innerJoin("cidadeestado", "cidadeestado.CidadeEstado_Id", "endereco.Endereco_CidadeEstado_Id")
		.where("documento.Documento_TipoDocumento_Id", 6)
		.where("documento.Documento_Valor", cpf)
		.where("filiados.Filiado_Visivel", 1)
		.where("empresa.Empresa_Visivel", 1)
		.first();

	if (retorno) {
		return retorno;
	} else {
		throw 'Filiado(a) não encontrado !!';
	}

}

exports.login = async (cpf, senha, database) => {

	const conexaoFiliados = conexao.criarConexaoFiliados(database);

	const password = crypto.createHmac('md5', senha).digest('hex');

	const result = await conexaoFiliados('documento')
		.select({ cpf: "Documento_Valor" })
		.where("Documento_Filiado_Senha", password)
		.where("Documento_Valor", cpf)
		.where("Documento_TipoDocumento_Id", 6)
		.first();

	if (result) {
		return result;
	} else {
		throw 'Usuario ou Senha invalidos !!';
	}
}

exports.verificarPrimeiroLogin = async (cpf, database) => {

	const conexaoFiliados = await conexao.criarConexaoFiliados(database);

	const result = await conexaoFiliados('documento')
		.select({ cpf: "documento.Documento_Valor" })
		.innerJoin("filiadoshasdocumento", "filiadoshasdocumento.Documento_Documento_Id", "documento.Documento_Id")
		.innerJoin("filiados", "filiados.Filiado_Id", "filiadoshasdocumento.Filiados_Filiado_Id")
		.innerJoin("pessoafisica", "pessoafisica.PessoaFisica_Id", "filiados.Filiado_PessoaFisica_Id")
		.innerJoin("dadossindicato", "dadossindicato.Filiados_Filiado_Id", "filiados.Filiado_Id")
		.innerJoin("filiadoshasempresa", "filiadoshasempresa.Filiados_Filiado_Id", "filiados.Filiado_Id")
		.innerJoin("empresa", "filiadoshasempresa.Empresa_Empresa_Id", "empresa.Empresa_Id")
		.where("documento.Documento_TipoDocumento_Id", 6)
		.where("documento.Documento_Valor", cpf)
		.where("filiados.Filiado_Visivel", 1)
		.where("empresa.Empresa_Visivel", 1)
		.where("documento.Documento_Filiado_Senha", null)
		.first();
		
	if (result) {
		return true;
	} else {
		return false;
	}
}

exports.atualizarSenha = async (cpf, senha, database, novaSenha) => {

	const conexaoFiliados = conexao.criarConexaoFiliados(database);

	const password = crypto.createHmac('md5', senha).digest('hex');

	if (novaSenha !== undefined) {
		const newPassword = crypto.createHmac('md5', novaSenha).digest('hex');

		const result = await conexaoFiliados('documento')
			.where("Documento_Valor", cpf)
			.where("Documento_TipoDocumento_Id", 6)
			.where("Documento_Filiado_Senha", password)
			.update({ Documento_Filiado_Senha: newPassword });

		if (result) {
			return true;
		} else {
			throw 'Falha na Atualização da senha.';
		}
	} else {
		const result = await conexaoFiliados('documento')
			.where("Documento_Valor", cpf)
			.where("Documento_TipoDocumento_Id", 6)
			.update({ Documento_Filiado_Senha: password });

		if (result) {
			return true;
		} else {
			throw 'Falha na Atualização da senha.';
		}
	}

}

exports.atualizarCodigoRecorrencia = async (codigoRecorrencia, body) => {

	const { database } = await sindicatoModel.getSindicato(body.idSindicato);

	const conexaoFiliados = await conexao.criarConexaoFiliados(database);
	const result = await conexaoFiliados('documento')
		.where("Documento_Valor", body.cpf)
		.where("Documento_TipoDocumento_Id", 6)
		.update({ Documento_Filiado_Recorrencia: codigoRecorrencia });

	if (result) {
		return true;
	} else {
		throw 'Erro ao atualizar o codigo da Recorrencia.';
	}
}

exports.consultarCodigoRecorrencia = async (cpf, database) => {

	const conexaoFiliados = conexao.criarConexaoFiliados(database);

	const result = await conexaoFiliados('documento')
		.select({ codigoRecorrencia: "Documento_Filiado_Recorrencia" })
		.where("documento.Documento_TipoDocumento_Id", 6)
		.where("documento.Documento_Valor", cpf)
		.first();

	if (result) {
		return true;
	} else {
		return false;
	}

 }
