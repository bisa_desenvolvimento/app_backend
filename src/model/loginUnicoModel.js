const conexao = require('../database/baseFiliados');
const conexaoMoneta = require('../database/baseMoneta');

exports.getUsuario = async (idCliente, ambiente, idUsuario) => {

    if (ambiente == 'teste'){
        database = 'bisaweb_geral_teste'
    } else {
        database = 'bisaweb_geral'
    }

    const conexaoLoginUnico = await conexao.criarConexaoFiliados(database);
    // Mantis 19276
     let perfilUsuario;
     let acesso_filiados;
     let base_filiados;
     let eh_preview;

     if (idUsuario) {
         const usuario = await conexaoLoginUnico('usuarios')
             .select('perfil_filiados_id')
             .where('id', idUsuario)
             .first();
         perfilUsuario = usuario ? usuario.perfil_filiados_id : null;
         
        const acesso_filiadosweb = await conexaoLoginUnico("usuarios")
            .select("acesso_filiadosweb")
            .where("id", idUsuario)
            .first();
        acesso_filiados = acesso_filiadosweb ? acesso_filiadosweb.acesso_filiadosweb : null;

        if (acesso_filiados && (perfilUsuario == 2 || perfilUsuario == 3 || perfilUsuario == 4 || perfilUsuario == 5 )) {
            const baseFiliados = await conexaoLoginUnico("cliente_link")
                .select("Clie_Database")
                .where("clie_Codigo", idCliente)
                .first();
            base_filiados = baseFiliados ? baseFiliados.Clie_Database : null;

            const conexaofiliados = await conexao.criarConexaoFiliados(base_filiados);
            
            const perfil_fw_permissao = await conexaofiliados("perfilhasmenu")
            .select("Perfil_Perfil_Id")
            .where("Menu_Menu_ID", 71)
            .where("Perfil_Perfil_Id", perfilUsuario)
            .first();
            eh_preview = perfil_fw_permissao ? true : false;
        }
        
     }

    // Mantis 19276
    if (idUsuario == 1 || perfilUsuario == 1){
        retorno = await conexaoLoginUnico('usuarios')
        .select(
            { id: 'usuarios.id'},
            { email: 'usuarios.email'},
            { telefone: 'usuarios.telefone'},
            { nome: 'usuarios.nome'},
            { codigo: 'usuarios.cliente_codigo'},
            { perfilMoneta: 'usuario_perfil.descricao'},
            { perfilFiliado: 'fw_perfil.perfil_nome'},
            { perfilNoticiador: 'nw_perfil.perfil_nome'},
            { acessoMoneta: 'usuarios.acesso_monetaweb'},
            { acessoFiliado: 'usuarios.acesso_filiadosweb'},
            { acessoNoticiador: 'usuarios.acesso_noticiadorweb'},
            { usuario_habilitado: 'usuarios.usuario_habilitado'} )
        .innerJoin('usuario_perfil', 'usuario_perfil.id', 'usuarios.perfil_id')
        .innerJoin('fw_perfil', 'fw_perfil.id', 'usuarios.perfil_filiados_id')
        .leftJoin('nw_perfil', 'nw_perfil.id', 'usuarios.perfil_noticiador_id')
        .where('deleted_at', null);
    } else if (acesso_filiados && (perfilUsuario == 2 || perfilUsuario == 3 || perfilUsuario == 4 || perfilUsuario == 5 )) {
        if (eh_preview) {
            retorno = await conexaoLoginUnico('usuarios')
            .select(
                    { id: 'usuarios.id'},
                    { email: 'email'},
                    { nome: 'nome'},
                    {telefone: 'telefone'},
                    { codigo: 'usuarios.cliente_codigo'},
                    { perfilMoneta: 'usuario_perfil.descricao'},
                    { perfilFiliado: 'fw_perfil.perfil_nome'},
                    { perfilNoticiador: 'nw_perfil.perfil_nome'},
                    { acessoMoneta: 'usuarios.acesso_monetaweb'},
                    { acessoFiliado: 'usuarios.acesso_filiadosweb'},
                    { acessoNoticiador: 'usuarios.acesso_noticiadorweb'})
            .innerJoin('usuario_perfil', 'usuario_perfil.id', 'usuarios.perfil_id')
            .innerJoin('fw_perfil', 'fw_perfil.id', 'usuarios.perfil_filiados_id')
            .leftJoin('nw_perfil', 'nw_perfil.id', 'usuarios.perfil_noticiador_id')
                .where('deleted_at', null)
                .where('cliente_codigo', idCliente);
        } else {
            retorno = await conexaoLoginUnico('usuarios')
            .select(
                    { id: 'usuarios.id'},
                    { email: 'email'},
                    { nome: 'nome'},
                    {telefone: 'telefone'},
                    { codigo: 'usuarios.cliente_codigo'},
                    { perfilMoneta: 'usuario_perfil.descricao'},
                    { perfilFiliado: 'fw_perfil.perfil_nome'},
                    { perfilNoticiador: 'nw_perfil.perfil_nome'},
                    { acessoMoneta: 'usuarios.acesso_monetaweb'},
                    { acessoFiliado: 'usuarios.acesso_filiadosweb'},
                    { acessoNoticiador: 'usuarios.acesso_noticiadorweb'} )
            .innerJoin('usuario_perfil', 'usuario_perfil.id', 'usuarios.perfil_id')
            .innerJoin('fw_perfil', 'fw_perfil.id', 'usuarios.perfil_filiados_id')
            .leftJoin('nw_perfil', 'nw_perfil.id', 'usuarios.perfil_noticiador_id')
                .where('deleted_at', null)
                .where('usuarios.id', idUsuario);
        }
    } else {
        retorno = await conexaoLoginUnico('usuarios')
            .select(
                { id: 'usuarios.id'},
                { email: 'email'},
                { nome: 'nome'},
                { telefone: 'telefone'},
                { codigo: 'usuarios.cliente_codigo'},
                { perfilMoneta: 'usuario_perfil.descricao'},
                { perfilFiliado: 'fw_perfil.perfil_nome'},
                { perfilNoticiador: 'nw_perfil.perfil_nome'},
                { acessoMoneta: 'usuarios.acesso_monetaweb'},
                { acessoFiliado: 'usuarios.acesso_filiadosweb'},
                { acessoNoticiador: 'usuarios.acesso_noticiadorweb'},
                { usuario_habilitado: 'usuarios.usuario_habilitado'} )
        .innerJoin('usuario_perfil', 'usuario_perfil.id', 'usuarios.perfil_id')
        .innerJoin('fw_perfil', 'fw_perfil.id', 'usuarios.perfil_filiados_id')
        .leftJoin('nw_perfil', 'nw_perfil.id', 'usuarios.perfil_noticiador_id')
            .where('deleted_at', null)
            .where('cliente_codigo', idCliente);
    }

    console.log("Fetched permission:", acesso_filiados);
    console.log("preview permission:", eh_preview);
    if (retorno){
        return retorno;
    } else {
        throw 'Sem Usuarios Cadastrados';
    }
}

exports.getCliente = async (idCliente) => {
    const result = await conexaoMoneta('acrwclie')
        .select(
            { nome: 'CLIE_Nome'})
        .where('CLIE_Codigo', idCliente)
        .where('CLIE_Visivel', 1)
        .first();
        
    let data = { ...result}

    return data.nome || '';
}
