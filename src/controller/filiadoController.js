const filiadoModel = require('../model/filiadoModel');
const sindicatoModel = require('../model/sindicatoModel');
const jwt = require('../config/authenticate');

exports.login = async(req, res) =>{

    const {cpf, senha, idSindicato} = req.body;

    var data

    try {
        const sindicato = await sindicatoModel.getSindicato(idSindicato);

        const firstLogin = await filiadoModel.verificarPrimeiroLogin(cpf, sindicato.database);

        if(firstLogin && senha === cpf) {
            data = await filiadoModel.atualizarSenha(cpf, cpf, sindicato.database);
        } else {
            data = await filiadoModel.login(cpf,senha, sindicato.database);
        }
        
        const result = await filiadoModel.getFiliado( cpf, sindicato.database);

        const token = await jwt.getToken(result);
        
        if(data){
            return res.json({
                usuario: result,
                token: token,
                primeiroLogin: firstLogin
            });
        }

    } catch (error) {
        return res.status(400).json({
            error
        })
    }
}

exports.alterarSenha = async(req, res) => {
    
    const { cpf, senha, idSindicato, novaSenha } = req.body;

    try {
        const sindicato = await sindicatoModel.getSindicato(idSindicato);

        data = await filiadoModel.atualizarSenha(cpf, senha, sindicato.database, novaSenha);

        if (data) {
            return res.status(200).send({ sucess: 'Senha alterada com sucesso'});
        }

    } catch (error) {
        return res.status(400).json({
            error
        })
    }
}

exports.consultarFiliado = async (req, res ) => {

    const { cpf, idSindicato} = req.params;

    try {

        const sindicato = await sindicatoModel.getSindicato(idSindicato);

        const result = await filiadoModel.getFiliado( cpf, sindicato.database);

        return res.json(result);

    } catch (error) {
        return res.status(400).json(error);
    }

}


