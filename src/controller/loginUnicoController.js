const loginUnicoModel = require('../model/loginUnicoModel');

exports.getUsuarios = async (req, res) => {

    const {idSindicato, ambiente, idUsuario} = req.params;

    try {
        const result = await loginUnicoModel.getUsuario(idSindicato, ambiente, idUsuario);
   
        const resultado = await Promise.all(result.map(async item => {
           
            let cliente = await loginUnicoModel.getCliente(item.codigo);
        
            return {...item, cliente} 
        }))
        
        return res.json({data: resultado});
        
    } catch (error) {
        return res.status(400).json(error);
    }
}