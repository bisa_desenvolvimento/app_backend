const sindicatoModel = require('../model/sindicatoModel');

exports.getSindicato = async(req, res) => {
    
    const {id} = req.params;

    try {

        const result = await sindicatoModel.getSindicato(id);

        res.json(result);
        
    } catch (error) {
        return res.status(400).json({
            error
        })
    }
}

exports.listSindicato = async(req, res) => {
    
    try {
        const result = await sindicatoModel.listSindicato();

        res.json(result);

    } catch (error) {
        return res.status(400).json({
            error
        })
    }
}