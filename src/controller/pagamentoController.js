const pagarme = require('pagarme');

const sindicatoModel = require('../model/sindicatoModel');
const filiadoModel = require('../model/filiadoModel');

const getClient = async () => {
    // return await pagarme.client.connect({ api_key: 'ak_test_J4flRIBAiOH6OQESkofFTqZduHdahu' });
    return await pagarme.client.connect({ api_key: 'ak_live_9C1n6OVxAeHqLGeARdp4LTjVswFvB0' });
}

exports.pagamento = async (req, res) => {

    try {
        const valor = await calcularValorRecorrencia(req.body.salario);

        const planos = await consultarPlanos();

        const plano = planos.find(plano => plano.amount == valor);

        if (plano) {
            const assinatura = await criarAssinatura(plano.id, req.body);
            await filiadoModel.atualizarCodigoRecorrencia(assinatura.id, req.body);
            return res.json(assinatura);

        } else {
            const novoPlano = await criarPlano(valor);
            const assinatura = await criarAssinatura(novoPlano.id, req.body);
            await filiadoModel.atualizarCodigoRecorrencia(assinatura.id, req.body);
            return res.json(assinatura);
        }
    } catch (error) {

        if (error.response){
            const { status, errors } = error.response;
            return res.status(status).json({ message: errors[0].message });
        } else {
            return res.status(400).json({ message: error });

        }
    }

}

exports.alterarRecorrencia = async (req, res) => {
    
    try {
        const valor = await calcularValorRecorrencia(req.body.salario);

        const planos = await consultarPlanos();

        const plano = planos.find(plano => plano.amount == valor);

        if (plano) {
            const assinatura = await alterarAssinatura(plano.id, req.body);
            return res.json(assinatura);

        } else {
            const novoPlano = await criarPlano(valor);
            const assinatura = await alterarAssinatura(novoPlano.id, req.body);
            return res.json(assinatura);
        }               

    } catch (error) {
        const { status, errors } = error.response;
        return res.status(status).json({ message: errors[0].message });
    }
}

exports.checarRecorrencia = async (req, res) => {
    
    try {
        const client = await getClient();
        const result = await client.subscriptions.find({
            id: req.params.recorrenciaId
        });

        return res.json(result);
       
    } catch (error) {
        const { status, errors } = error.response;
        return res.status(status).json({ message: errors[0].message });
    }
}

async function criarPlano(valor) {

    if (valor < 998) {
        throw 'Você não pode informar um valor menor que o salário mínimo atual.';
    }

    const client = await getClient();
    const result = await client.plans.create({
        amount: valor,
        days: 30,
        name: `Plano: ${valor}`,
        payments_methods: ['credit_card']
    });
    return result;

}

async function consultarPlanos() {

    const client = await getClient();
    const result = await client.plans.all();
    return result;

}

async function criarAssinatura(idPlano, body) {

    const client = await getClient();

    const cardHash = await criarCardHash(body);

    const regraSplit = await criarSplit(body);

    const assinatura = await client.subscriptions.create({
        plan_id: idPlano,
        card_number: body.card_number,
        card_holder_name: body.card_holder_name,
        card_expiration_date: body.card_expiration_date,
        card_cvv: body.card_cvv,
        card_hash: cardHash,
        customer: {
            email: body.customer.email,
            document_number: body.cpf,
            name: body.nome,
            address: {
                street: body.customer.address.street,
                street_number: body.customer.address.street_number,
                neighborhood: body.customer.address.neighborhood,
                zipcode: body.customer.address.zipcode
            }
        },
        split_rules: regraSplit,
    });
    return assinatura;
}

async function alterarAssinatura(){

    const client = await getClient();

    const cardHash = await criarCardHash(body);

    const assinatura = await client.subscriptions.create({
        plan_id: idPlano,
        card_number: body.card_number,
        card_holder_name: body.card_holder_name,
        card_expiration_date: body.card_expiration_date,
        card_cvv: body.card_cvv,
        card_hash: cardHash,
        payment_method: "credit_card"
    });

    return assinatura;

}

async function calcularValorRecorrencia(valorAnterior) {

    if (valorAnterior < 998) {
        throw 'Você não pode informar um valor menor que o salário mínimo atual.';
    }

    let valor = (valorAnterior / 100);

    if (valor < 10) valor = 10;

    valor = valor.toFixed(2);
    valor = valor.replace(".", "");

    return valor;
}

async function criarCardHash(body) {

    const client = await getClient();

    const cardHash = await client.security.encrypt({
        card_number: body.card_number,
        card_holder_name: body.card_holder_name,
        card_expiration_date: body.card_expiration_date,
        card_cvv: body.card_cvv,
    });

    return cardHash;
}

async function criarSplit(body) {

    const { percentBisa, recipientId } = await sindicatoModel.getSindicato(body.idSindicato);

    const percentCliente = (100 - percentBisa);

    const split_rules = [
        {
            recipient_id: recipientId, //'re_cjwkseput03f6pm6e1f4pts8a' 
            charge_processing_fee: true,
            liable: true,
            percentage: percentCliente
        },
        {
            // recipient_id: "re_cjud1q4po0iqcxi6dfl05jbnm", // id producao re_cjud1x5jn07sp925xcbtavcap
            recipient_id: "re_cjud1x5jn07sp925xcbtavcap",
            charge_processing_fee: false,
            liable: false,
            percentage: percentBisa
        }
    ];

    return split_rules;
}
