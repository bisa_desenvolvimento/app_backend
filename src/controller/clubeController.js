const clubeModel = require('../model/clubeModel');

exports.index = async (req, res) => {

    const { filtro } = req.query

    try {

        let result = await clubeModel.getParceiros();

        if (filtro === "destaque") {
            result = result.filter(function(parceiro) {
                return parceiro.credenciadoClube === "D"
            })
        }

        const parceiros = result.map(function (r) {
            r.imagem = r.imagem ? `http://bisaweb.monetaweb.com.br/assents/${r.imagem.toString()}` : null;
            return r;
        })

        return res.json(parceiros);

    } catch (error) {
        console.log(error)
        return res.status(400).json({
            error
        })
    }
}

exports.show = async (req, res) => {
    const { id } = req.params;

    try {
        const parceiro = await clubeModel.findOne(id);

        parceiro.imagem = parceiro.imagem ? `http://bisaweb.monetaweb.com.br/assents/${parceiro.imagem.toString()}` : null;
        parceiro.vantagens = parceiro.vantagens.split("\r\n").filter(str => str);

        return res.json(parceiro);
    } catch (error) {
        console.log(error)
        return res.status(400).json({
            error
        })
    }
}