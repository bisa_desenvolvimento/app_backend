// Importa e Starta o ExpressJs
var express = require('express');
var app = express();
app.use(express.json());

var cors = require('cors');
app.use(cors());

app.use('/', require('./src/routes'));

app.use(express.urlencoded({
	extends: true
}));

app.set('port', process.env.PORT || 80);

app.listen(app.get('port'), function() {
	console.log('Start!!!, Port: ' + app.get('port'));
});








